## v0.1.1 / 2020-02-23

### 改进功能

* [更新提示改进](https://github.com/88250/liandi/issues/23)
* [新建文档后选中并打开](https://github.com/88250/liandi/issues/24)
* [macOS 应用菜单细化](https://github.com/88250/liandi/issues/26)
* [多主题改进](https://github.com/88250/liandi/issues/27)

### 修复缺陷

* [工具提示被遮挡](https://github.com/88250/liandi/issues/22)

## v0.1.0 / 2020-02-21

实现笔记应用基础功能，第一次公开发布。
